" Vim filetype detect for Amateur Data Interchange Format (ADIF).
" Last Change: 	2021 September 13
" Maintainer:	Nate Bargmann <n0nb@n0nb.us>
" License:	MIT-0
"
" A file format for exchanging amateur radio logging data between programs and
" various online services for radio amateurs.
"
" See http://www.adif.org/ for details on ADIF

au BufRead,BufNewFile *.{adif\=\c}	set filetype=adif

" Should probably include some sort of tag detection in case of file naming
" differences.  The only tags that seem to be reliable are <eoh> and <eor>
" (text in tags is not case sensitive).
