# ADIF support for Vim

File detect and syntax definition files for Amateur Data Interchange Format
(ADIF), a file format for exchanging amateur radio logging data between
programs and various online services for radio amateurs.

See http://www.adif.org/ for details on ADIF and its specification.

## Installation

In Vim 8+, follow its native package installation procedure:

```
:help package
```

Otherwise, use your favorite package manager.

## File detection

File names ending in `adi` or `adif` will be considered ADIF files.  No other
file detection is present at this time as no set of tags are required except
`<eoh>` and `<eor>`, although `<call:` may be an obvious choice and `<eor>`
another.

## Syntax highlighting

The syntax highlighting is simple:
- Highlight any lines not beginning with `<` (plus optional leading space) as
  comments.
- Highlight `eoh` and `eor` as keywords.
- Highlight the tag names (C 'words', i.e. A-Z (case insensitive), 0-9, and
  `_`) from `<` to the first colon as identifiers.
- Highlight the data length numerals as numbers.
- Highlight the optional data type characters as defines.
- Highlight the data after the tag's closing `>` as strings.

If these color choices are not suitable, simply change the linked color group
at the bottom of `syntax/adif.vim` in the rightmost column to color groups of
your preference.

**Hint**:  The

```
:highlight
```

command will show all the color groups and their colors combinations for your
chosen color scheme.

## Known bugs

Any header line that begins with a single word followed by a colon is
highlighted as an Identifier and not as a comment (Identifiers should have a
`<` immediately preceding the word).

## Motivation

I find myself editing an ADIF file from time to time and as there is no
specified order for the tags in each record, it's easy for me to get lost with
a literal screen full of monochrome lines.  This little bit of color lets me
find where I need to edit a bit more quickly.

73, Nate, N0NB

