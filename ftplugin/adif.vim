" Vim filetype plugin for Amateur Data Interchange Format (ADIF).
" Last Change: 	2021 September 13
" Maintainer:	Nate Bargmann <n0nb@n0nb.us>
" License:	MIT-0
"
" A file format for exchanging amateur radio logging data between programs and
" various online services for radio amateurs.
"
" See http://www.adif.org/ for details on ADIF

" Set some arbitrary format style.
setlocal tabstop=2
setlocal softtabstop=2
setlocal shiftwidth=2
